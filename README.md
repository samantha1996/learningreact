# Learning react 

in this project, I used react to create my very own frontend for a fake API. 

## How to run 

1. Clone this repository : 
``` git clone https://gitlab.com/samantha1996/learningreact``` 
2. Install dependencies : 
``` npm install ``` 
3. Run server: 
``` npm start ```

